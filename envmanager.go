package envmanager

import (
	"os"

	"github.com/rs/zerolog/log"

	"github.com/joho/godotenv"
)

type EnvVarManager struct {
	Debug       bool
	EnvFilePath string
}

// Loads the Environment Variables from the environment file that is passed as an argument. The path is function relative. The default file name is .env
// if there is a  DEBUG variable in the environment file, it will be used to set the debug mode.
func NewEnvVarManager(envFilePath string) *EnvVarManager {

	mydir, err := os.Getwd()
	if err != nil {
		log.Fatal().Err(err).Msg("os.Getwd() failed")
	}
	log.Info().Msgf("os.Getwd() = %s", mydir)

	err = godotenv.Load(envFilePath)

	if err != nil {
		log.Warn().Err(err).Msgf("Some error occured while loading the Environment Variables. Err: %s", err)
		return nil
	}

	debugMode := os.Getenv("DEBUG") == "true"

	return &EnvVarManager{
		Debug:       debugMode,
		EnvFilePath: envFilePath,
	}

}

// Returns the value of the environment variable. If the variable is not set, it returns an empty string.
// If the debug mode is set, it will log the variable name and value.
func (em *EnvVarManager) GetEnvironmentVariable(variableName string) string {
	val := os.Getenv(variableName)
	if em.Debug {
		log.Printf("Getting Environment Variable: %s, has value: %s", variableName, val)
	}
	return val
}
